package ru.bsa.Prototype;

public interface Copyable {
    Object clone();
}

package ru.bsa.Prototype;

public class StartPrototype {
    public static void main(String[] args) {

        Project projectOrigin = new Project(1, "project+","sourcecode");

        System.out.println(projectOrigin);

        ProjectFactory projectFactory = new ProjectFactory(projectOrigin);
        Project projectClone = projectFactory.cloneProject();

        System.out.println(projectClone);
    }
}

package ru.bsa.Singleton;

public class StartSingleton {
    public static void main(String[] args) {

        Singleton singleton1 = Singleton.createOrGetSingleton("Sergey");
        Singleton singleton2 = Singleton.createOrGetSingleton("Bob");

        System.out.println(singleton1 + " : " + singleton2);
    }
}

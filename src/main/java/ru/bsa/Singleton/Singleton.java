package ru.bsa.Singleton;

public class Singleton {
    private static Singleton singleton;
    private String name;
    private Singleton(String name) {
        this.name = name;
    }
    public static Singleton createOrGetSingleton(String name){
        if (singleton == null) singleton = new Singleton(name);
        return singleton;
    }
}

package ru.bsa.structuralDesignPatterns.Proxy;

public interface Project {
    public  void run();
}

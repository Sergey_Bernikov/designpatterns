package ru.bsa.structuralDesignPatterns.Proxy;

public class ProjectRunner {

    public static void main(String[] args) {
        Project project = new ProxyProject("http://www.github.com/realproject");
        project.run();
    }
}

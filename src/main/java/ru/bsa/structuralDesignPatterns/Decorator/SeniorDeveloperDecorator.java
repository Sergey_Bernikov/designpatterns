package ru.bsa.structuralDesignPatterns.Decorator;

public class SeniorDeveloperDecorator extends DeveloperDecorator{
    public SeniorDeveloperDecorator(Developer developer) {
        super(developer);
    }
    public String makeCodeReview(){
        return "Make code Review.";
    }

    @Override
    public String makejob() {
        return super.makejob() + makeCodeReview();
    }
}

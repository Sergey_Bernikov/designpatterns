package ru.bsa.structuralDesignPatterns.Decorator;

public class JavaTeamLead extends DeveloperDecorator{

    Developer developer;

    public JavaTeamLead(Developer developer) {
        super(developer);
    }

    String sendWeekReport(){
        return "Send week report to customer.";
    }
    @Override
    public String makejob() {
        return super.makejob() + sendWeekReport();
    }
}

package ru.bsa.structuralDesignPatterns.Decorator;

public class JavaDeveloper implements Developer{
    @Override
    public String makejob() {
        return "Java developer writes Java code...";
    }
}

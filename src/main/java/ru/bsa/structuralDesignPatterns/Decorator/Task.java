package ru.bsa.structuralDesignPatterns.Decorator;

public class Task {
    public static void main(String[] args) {
        Developer developer = new JavaTeamLead(new SeniorDeveloperDecorator(new JavaDeveloper()));
        System.out.println(developer.makejob());
    }
}

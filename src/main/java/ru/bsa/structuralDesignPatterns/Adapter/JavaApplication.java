package ru.bsa.structuralDesignPatterns.Adapter;

public class JavaApplication {

    public void saveObject(){
        System.out.println("Saving object object...");
    }
    public void updateObject(){
        System.out.println("Updating object object...");
    }
    public void loadObject(){
        System.out.println("Loading object object...");
    }
    public void deleteObject(){
        System.out.println("Deleting object object...");
    }

}

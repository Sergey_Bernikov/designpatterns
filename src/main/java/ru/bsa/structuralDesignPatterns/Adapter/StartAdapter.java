package ru.bsa.structuralDesignPatterns.Adapter;

public class StartAdapter {
    public static void main(String[] args) {

        DataBase dataBase = new AdapterJavaToDateBase();
        dataBase.insert();
        dataBase.update();
        dataBase.select();
        dataBase.remove();
    }
}

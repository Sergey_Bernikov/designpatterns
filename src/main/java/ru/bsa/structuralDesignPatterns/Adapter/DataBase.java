package ru.bsa.structuralDesignPatterns.Adapter;

public interface DataBase {

    void insert();

    void update();

    void select();

    void remove();
}

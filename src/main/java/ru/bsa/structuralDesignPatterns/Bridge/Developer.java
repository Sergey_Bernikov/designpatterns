package ru.bsa.structuralDesignPatterns.Bridge;

public interface Developer {
    void writeCode();
}

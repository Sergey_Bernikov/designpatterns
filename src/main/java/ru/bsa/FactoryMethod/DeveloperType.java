package ru.bsa.FactoryMethod;

public enum DeveloperType {
    JAVA("JAVA"),
    Cpp("Cpp");
    String name;

    DeveloperType(String name) {
        this.name = name;
    }
}

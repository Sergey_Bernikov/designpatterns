package ru.bsa.FactoryMethod;

public interface DeveloperFactory {
    Developer createDeveloper();
}

package ru.bsa.FactoryMethod;

public interface Developer {
    void writeCode();
}

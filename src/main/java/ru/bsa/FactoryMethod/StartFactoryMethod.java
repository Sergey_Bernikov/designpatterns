package ru.bsa.FactoryMethod;

public class StartFactoryMethod {
    public static void main(String[] args) {

        DeveloperFactory developerFactory = createDeveloperFactory(DeveloperType.JAVA.name);
        Developer developer = developerFactory.createDeveloper();

        developer.writeCode();
    }

    static DeveloperFactory createDeveloperFactory(String developerType) {
        DeveloperFactory developerFactory = null;
        switch (developerType) {
            case ("Cpp"): {
                developerFactory = new CppDeveloperFactory();
                break;
            }
            case ("JAVA"): {
                developerFactory = new JavaDeveloperFactory();
                break;
            }
            default: throw new RuntimeException(developerType + " NO-no-no");
        }
        return developerFactory;
    }
}

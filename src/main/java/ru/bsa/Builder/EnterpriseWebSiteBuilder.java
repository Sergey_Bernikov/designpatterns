package ru.bsa.Builder;
/**
 * Корпоративный web-сайт
 */
public class EnterpriseWebSiteBuilder extends WebSiteBuilder{
    @Override
    public void buildName() {
        webSite.setName("Enterprise Web Site");
    }

    @Override
    public void buildCms() {
        webSite.setCms(Cms.ALIFRESCO);
    }

    @Override
    public void buildPrice() {
        webSite.setPrice(10000);
    }
}

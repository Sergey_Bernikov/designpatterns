package ru.bsa.Builder;

/**
 * Класс для конструирования web-сайта
 */
public abstract class WebSiteBuilder {
    public WebSite webSite;

    public void createWebSite(){
        webSite = new WebSite();
    }
    public abstract void buildName();
    public abstract void buildCms();
    public abstract void buildPrice();

    public WebSite getWebSite(){
        return webSite;
    }
}

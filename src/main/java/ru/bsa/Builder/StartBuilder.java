package ru.bsa.Builder;

public class StartBuilder {

    public static void main(String[] args) {

        Director directorVisitCardWebSiteBuilder = new Director();
        directorVisitCardWebSiteBuilder.setWebSiteBuilder(new visitCardWebSiteBuilder());
        WebSite webSiteVisitCardWebSiteBuilder = directorVisitCardWebSiteBuilder.buildWebSite();
        System.out.println(webSiteVisitCardWebSiteBuilder);


        Director directorEnterpriseWebSiteBuilder = new Director();
        directorEnterpriseWebSiteBuilder.setWebSiteBuilder(new EnterpriseWebSiteBuilder());
        WebSite webSiteEnterpriseWebSiteBuilder = directorEnterpriseWebSiteBuilder.buildWebSite();
        System.out.println(webSiteEnterpriseWebSiteBuilder);
    }
}

package ru.bsa.Builder;

/**
 * Определяет какой сайт создавать
 */
public class Director {
    private WebSiteBuilder webSiteBuilder;

    public void setWebSiteBuilder(WebSiteBuilder webSiteBuilder) {
        this.webSiteBuilder = webSiteBuilder;
    }

    public WebSite buildWebSite(){
        webSiteBuilder.createWebSite();
        webSiteBuilder.buildName();
        webSiteBuilder.buildCms();
        webSiteBuilder.buildPrice();

        WebSite webSite = webSiteBuilder.getWebSite();
        return webSite;
    }

}

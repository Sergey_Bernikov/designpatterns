package ru.bsa.Builder;

/**
 * Простой web-сайт
 */
public class visitCardWebSiteBuilder extends WebSiteBuilder{
    @Override
    public void buildName() {
        webSite.setName("Visit card");
    }

    @Override
    public void buildCms() {
        webSite.setCms(Cms.WORDPRESS);
    }

    @Override
    public void buildPrice() {
        webSite.setPrice(500);
    }
}

package ru.bsa.AbstractFactory.webSite;

import ru.bsa.AbstractFactory.ProjectManager;

public class WebSitePM implements ProjectManager {
    @Override
    public void manageProject() {
        System.out.println("Website PM manages Website project...");
    }
}

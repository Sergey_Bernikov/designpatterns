package ru.bsa.AbstractFactory.webSite;

import ru.bsa.AbstractFactory.Tester;

public class ManualTester implements Tester {
    @Override
    public void testCode() {
        System.out.println("Manual tester test website...");
    }
}

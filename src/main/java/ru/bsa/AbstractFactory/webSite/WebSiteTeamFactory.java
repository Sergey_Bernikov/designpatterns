package ru.bsa.AbstractFactory.webSite;

import ru.bsa.AbstractFactory.Developer;
import ru.bsa.AbstractFactory.ProjectManager;
import ru.bsa.AbstractFactory.ProjectTeamFactory;
import ru.bsa.AbstractFactory.Tester;

public class WebSiteTeamFactory implements ProjectTeamFactory {
    @Override
    public Developer getDeveloper() {
        return new PhpDeveloper();
    }

    @Override
    public Tester getTester() {
        return new ManualTester();
    }

    @Override
    public ProjectManager getProjectManager() {
        return new WebSitePM();
    }
}

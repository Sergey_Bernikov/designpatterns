package ru.bsa.AbstractFactory;

import ru.bsa.AbstractFactory.banking.BankingTeamFactory;
import ru.bsa.AbstractFactory.webSite.WebSiteTeamFactory;

public class StartAbstractFactory {
    public static void main(String[] args) {

        /* Создаем первую команду (банк) */
        ProjectTeamFactory projectTeamFactoryBank = new BankingTeamFactory();

        Developer developerBank = projectTeamFactoryBank.getDeveloper();
        Tester testerBank = projectTeamFactoryBank.getTester();
        ProjectManager projectManagerBank = projectTeamFactoryBank.getProjectManager();

        projectManagerBank.manageProject();
        developerBank.writeCode();
        testerBank.testCode();

        System.out.println("-------------------------------------------------------------");

        /* Создаем вторую команду (сайт) */

        ProjectTeamFactory projectTeamFactorySite = new WebSiteTeamFactory();

        Developer developerSite = projectTeamFactorySite.getDeveloper();
        Tester testerSite = projectTeamFactorySite.getTester();
        ProjectManager projectManagerSite = projectTeamFactorySite.getProjectManager();

        developerSite.writeCode();
        testerSite.testCode();
        projectManagerSite.manageProject();
    }
}

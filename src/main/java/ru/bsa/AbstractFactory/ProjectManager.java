package ru.bsa.AbstractFactory;

public interface ProjectManager {
    void manageProject();
}

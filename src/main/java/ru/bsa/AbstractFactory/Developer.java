package ru.bsa.AbstractFactory;

public interface Developer {
    void writeCode();
}

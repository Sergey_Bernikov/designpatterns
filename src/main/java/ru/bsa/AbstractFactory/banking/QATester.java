package ru.bsa.AbstractFactory.banking;

import ru.bsa.AbstractFactory.Tester;

public class QATester implements Tester {
    @Override
    public void testCode() {
        System.out.println("QA tester tests banking code...");
    }
}

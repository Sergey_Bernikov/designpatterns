package ru.bsa.AbstractFactory.banking;

import ru.bsa.AbstractFactory.Developer;
import ru.bsa.AbstractFactory.ProjectManager;
import ru.bsa.AbstractFactory.ProjectTeamFactory;
import ru.bsa.AbstractFactory.Tester;

public class BankingTeamFactory implements ProjectTeamFactory {
    @Override
    public Developer getDeveloper() {
        return new JavaDeveloper();
    }

    @Override
    public Tester getTester() {
        return new QATester();
    }

    @Override
    public ProjectManager getProjectManager() {
        return new BankingPM();
    }
}
